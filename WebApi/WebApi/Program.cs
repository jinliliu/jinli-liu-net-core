using WebApi.Common.RabbitMq;
using WebApi.Common.Redis;
using WebApi.Service.Configs;
using WebApi.Service.Users;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IUserService, UserService>();
//����log4net
builder.Logging.AddLog4Net("LogConfig/log4Net.config");
//ע��AutoMapper
builder.Services.AddAutoMapper(typeof(AutoMapperConfigs));
//ע��redis
builder.Services.AddSingleton<RedisClient>();
//ע��RabbitMQ
builder.Services.AddSingleton<RabbitMQClient>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
