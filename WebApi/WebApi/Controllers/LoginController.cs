﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Common;
using WebApi.Common.RabbitMq;
using WebApi.Common.Redis;
using WebApi.Model;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly RedisClient _redisClient;
        private readonly RabbitMQClient _rabbitMQClient;
        public LoginController(RedisClient redisClient, RabbitMQClient rabbitMQClient)
        {
            _redisClient = redisClient;
            _rabbitMQClient = rabbitMQClient;
        }
        [HttpGet]
        public IActionResult GetValidateCode(string t)
        {
            TestMQModel m = new TestMQModel();
            m.Id = 1;
            m.PassWord = "123456";
            m.Name = "张三";
            _rabbitMQClient.PushMessage("EMS", m);
            var db = _redisClient.Database;
            db.StringSet("Message", "你好小明");
           var str = db.StringGet("Message").ToString();
            Console.WriteLine(str);
            var validateStringCode = Tool.CreateValidateString();
             MemoryHelper.SetMemory(t, validateStringCode,1);
            byte[] buffer = Tool.DrawImage(validateStringCode);
            return File(buffer, @"image/png");
         }

        //[HttpGet]
        //public IActionResult GetTestMQ()
        //{
            
        //    var api = new ApiModel()
        //    {
        //        status = 200,
        //        data = m,
        //        msg = "成功"
        //    };
        //    return Ok(api);
        //}
    }
}
