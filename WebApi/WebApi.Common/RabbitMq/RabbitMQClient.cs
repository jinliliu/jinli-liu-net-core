﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Common.RabbitMq.Extensions;

namespace WebApi.Common.RabbitMq
{
    public class RabbitMQClient
    {
        private readonly IModel _channel;
        private readonly ILogger<RabbitMQClient> _logger;
        private readonly IConfiguration _configuration;
        private readonly ConnectionFactory _factory;
        private readonly IConnection _connection;
        public RabbitMQClient(IConfiguration configuration, ILogger<RabbitMQClient> logger)
        {
            _logger = logger;
            _configuration = configuration;
            _factory = new ConnectionFactory
            {
                Uri = _configuration.GetRabbitMQUri()
            };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare("WebAPI", ExchangeType.Fanout, true);
        }
        public virtual void PushMessage(string routingKey, object message)
        {
            _logger.LogInformation($"PushMessage,routingKey:{routingKey}");
            _channel.QueueDeclare(queue: "ApiQueue",
                                        durable: false,
                                        exclusive: false,
                                        autoDelete: false,
                                        arguments: null);
            string msgJson = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(msgJson);
            _channel.BasicPublish(exchange: "message",
                                    routingKey: routingKey,
                                    basicProperties: null,
                                    body: body);
        }
    }
}
