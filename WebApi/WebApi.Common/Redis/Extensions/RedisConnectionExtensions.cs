﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Common.Redis.Extensions
{
    public static class RedisConnectionExtensions
    {
        private const string CONFIGURATION_CONFIG_SECTION_KEY = "Redis:Configuration";
        public static string GetRedisConfigurationString(this IConfiguration configuration)
        {
            var conn = configuration.GetSection(CONFIGURATION_CONFIG_SECTION_KEY).Value;
            if (String.IsNullOrEmpty(conn))
            {
                throw new ArgumentNullException(CONFIGURATION_CONFIG_SECTION_KEY, "Redis 的 Configuration 未配置，请检查 appsettings.json 文件");
            }

            return conn;
        }
    }
}
