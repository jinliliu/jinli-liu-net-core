﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Common.Redis.Extensions;

namespace WebApi.Common.Redis
{
    public class RedisClient
    {
        private readonly ConnectionMultiplexer _conn;
        private readonly ILogger<RedisClient> _logger;
        private readonly IConfiguration _configuration;
        private const string CONFIGURATION_CONFIG_SECTION_KEY = "Redis:Configuration";
        public RedisClient(ILogger<RedisClient> logger,IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _conn = ConnectionMultiplexer.Connect(_configuration.GetRedisConfigurationString());
            
           
        }
        public IDatabase Database => _conn.GetDatabase();
    }
}
