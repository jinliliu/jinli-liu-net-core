﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Model.Entitys;

namespace WebApi.Service.Users
{
    public interface IUserService
    {
        Task<User> CheckLogin(); 
    }
}
