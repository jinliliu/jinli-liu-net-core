﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Common.DbCommon;
using WebApi.Model.Entitys;

namespace WebApi.Service.Users
{
    public class UserService : IUserService
    {
        public async Task<User> CheckLogin()
        {
            return await DbContext.db.Queryable<User>().FirstAsync();
        }
    }
}
